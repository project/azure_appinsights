# Changelog

All notable changes to this project will be documented in this file. See [standard-version](https://github.com/conventional-changelog/standard-version) for commit guidelines.

## [1.0.0-alpha.3](https://git.drupalcode.org/project/azure_appinsights/compare/v1.0.0-alpha.2...v1.0.0-alpha.3) (2020-03-20)


### Bug Fixes

* **permissions:** use different base permission ([271f025](https://git.drupalcode.org/project/azure_appinsights/commit/271f025ff62179049f3a627979b0cb6aee17463e))

## [1.0.0-alpha.2](https://git.drupalcode.org/project/azure_appinsights/compare/v1.0.0-alpha.1...v1.0.0-alpha.2) (2020-03-20)

## [1.0.0-alpha.1](https://git.drupalcode.org/project/azure_appinsights/compare/v1.0.0...v1.0.0-alpha.1) (2020-02-18)


### Features

* **config:** add browserslist ([3c52248](https://git.drupalcode.org/project/azure_appinsights/commit/3c52248921b36799638a64b8b831161f90e7483e))
* **form:** add config form ([97a8885](https://git.drupalcode.org/project/azure_appinsights/commit/97a8885b1b76e2442ed85e51c916d1d53e7212cf))
* **library:** define a library ([33e597e](https://git.drupalcode.org/project/azure_appinsights/commit/33e597e21bef92a1ebc04a7521775ca8eb190918))
* **module:** use browser library for application ([2daae7c](https://git.drupalcode.org/project/azure_appinsights/commit/2daae7cdb3d266c96b0c9c7c331cdc3383c5b0ff))
* **php:** add compatibility test ([cd51723](https://git.drupalcode.org/project/azure_appinsights/commit/cd51723d4b6a5b3567bbd03326612a420b8a78b5))
* **version:** automatically update .info.yml file with standard-version ([298e8c9](https://git.drupalcode.org/project/azure_appinsights/commit/298e8c90b9145f24297e785fe5282991075b5a0d))


### Bug Fixes

* **build:** recompile ([c247ca5](https://git.drupalcode.org/project/azure_appinsights/commit/c247ca5a2c88de60771e52ed483cd8edc26925e3))
* **dist:** update backage ([b3b1dd6](https://git.drupalcode.org/project/azure_appinsights/commit/b3b1dd6f7f5530787546a8d6e4917a45935c35d0))
* **form:** change variable name ([c249f34](https://git.drupalcode.org/project/azure_appinsights/commit/c249f34641fb44ab72b2fdb9e3f9deffd7938b14))
* **insights:** fix spelling ([de1fa92](https://git.drupalcode.org/project/azure_appinsights/commit/de1fa92d791c939ed115268fb5cf19af3c8205d3))
* **js:** add proper wrapper ([c9740e1](https://git.drupalcode.org/project/azure_appinsights/commit/c9740e1b8a0a8f5c70a0999023b5f3fd5a5c47c1))
* **js:** don't add unless the key is set ([5d9c681](https://git.drupalcode.org/project/azure_appinsights/commit/5d9c681ecdd58a9bd279fb82b28c2303877db806))
* **js:** pass es6 ([9bba884](https://git.drupalcode.org/project/azure_appinsights/commit/9bba8845bafc75d5a4af4c029e9369c4e0e5725d))
* **js:** remove drupal variable ([6f7c441](https://git.drupalcode.org/project/azure_appinsights/commit/6f7c441a3cb49a89c2576e2096fe9a2cc48ef301))
* **js:** use drupal settings ([a5440ea](https://git.drupalcode.org/project/azure_appinsights/commit/a5440eaab78887b42dcb094d117d9d6438b1288c))
* **key:** use object ([6dab5da](https://git.drupalcode.org/project/azure_appinsights/commit/6dab5daeae81111ba39efe025d7ff33708f8298f))
* **version:** add package files ([a628b1b](https://git.drupalcode.org/project/azure_appinsights/commit/a628b1b1af2951dc603fc2f2729768df4a3daf03))
* **version:** change update to string ([d041e5b](https://git.drupalcode.org/project/azure_appinsights/commit/d041e5bc85a521c7a9949079907f356339a730f7))
* **version:** use proper package and bump file setup ([1eb77be](https://git.drupalcode.org/project/azure_appinsights/commit/1eb77be95a4feb1066c50e69f006d010a1d4be81))
* **xss:** add sanitization ([6b9ccdb](https://git.drupalcode.org/project/azure_appinsights/commit/6b9ccdbe01eb7221c13e8656b5d4df110be9e83c))


### Build System

* **gulp:** update deps ([9810d15](https://git.drupalcode.org/project/azure_appinsights/commit/9810d15f9e3a72ed28e31721df135fbb9665076f))
* update package info ([b42c198](https://git.drupalcode.org/project/azure_appinsights/commit/b42c19874b73a85767af8fc86b63737d0dd67e1f))
* **npm:** add @microsoft/applicationinsights-web ([aac7bbf](https://git.drupalcode.org/project/azure_appinsights/commit/aac7bbf6a1f16b9b749a0356c1444e5e188b57f4))
* **npm:** run updates ([7332c33](https://git.drupalcode.org/project/azure_appinsights/commit/7332c339dea67ba1e2a7eb31179183e358658d22))
* **npm:** update init script to use ci if possible ([f4216e7](https://git.drupalcode.org/project/azure_appinsights/commit/f4216e7aee307bddbce266a9abefacb5cc6a4c2d))


### Continuous Integration

* **templates:** add environments template ([cc9c3c1](https://git.drupalcode.org/project/azure_appinsights/commit/cc9c3c1eb674bd3654ac4318376a3bb05811bfad))
* **templates:** update to 2.0.0 ([6e36552](https://git.drupalcode.org/project/azure_appinsights/commit/6e365520a396df63a6a413627cf795eae4a29a1d))
